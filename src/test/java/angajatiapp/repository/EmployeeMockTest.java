package angajatiapp.repository;

import angajatiapp.controller.DidacticFunction;
import angajatiapp.controller.EmployeeController;
import angajatiapp.model.Employee;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EmployeeMockTest {

    EmployeeMock employeeMock = new EmployeeMock();
    Employee e = null;

    @Order(1)
    @Test
    void addEmployee_TC1_BB() {
//        TC1_EC	TC10_BVA	Ciolpan	Diana	2921120124598	LECTURER	5000	TRUE

        Employee e = new Employee();
        e.setLastName("Ciolpan");
        e.setFirstName("Diana");
        e.setCnp("2921120124598");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(5000.0);

        assertTrue(employeeMock.addEmployee(e));
    }

    @Order(2)
    @Test
    void addEmployee_TC2_BB() {
//        TC2_EC		Ciolpan	diana	292abcd124598	LECTURER	5000	FALSE

        Employee e = new Employee();
        e.setLastName("Ciolpan");
        e.setFirstName("diana");
        e.setCnp("292abcd124598");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(5000.0);

        assertFalse(employeeMock.addEmployee(e));
    }

    @Order(3)
    @Test
    void addEmployee_TC3_BB() {
//       TC3_EC	TC_9_BVA	Ciolpan	Diana	292112025547	LECTURER	5000	FALSE

        Employee e = new Employee();
        e.setLastName("Ciolpan");
        e.setFirstName("Diana");
        e.setCnp("292112025547");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(5000.0);

        assertFalse(employeeMock.addEmployee(e));
    }

    @Order(4)
    @Test
    void addEmployee_TC4_BB() {
//      TC4_EC		Ciolpan	8iana	292112012458	LECTURER	5000	FALSE

        Employee e = new Employee();
        e.setLastName("Ciolpan");
        e.setFirstName("8iana");
        e.setCnp("292112012458");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(5000.0);

        assertFalse(employeeMock.addEmployee(e));
    }


    @Order(5)
    @Test
    void addEmployee_TC5_BB() {
//        TC5_EC		%iolpan	Diana	2921120124598	LECTURER	5000	FALSE

        Employee e = new Employee();
        e.setLastName("%iolpan");
        e.setFirstName("Diana");
        e.setCnp("2921120124598");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(5000.0);

        assertFalse(employeeMock.addEmployee(e));
    }

    @Order(6)
    @Test
    void addEmployee_TC6_BB() {
//       TC1_BVA	@iolpan	Diana	2921120124598	LECTURER	5000	FALSE

        Employee e = new Employee();
        e.setLastName("@iolpan");
        e.setFirstName("Diana");
        e.setCnp("2921120124598");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(5000.0);

        assertFalse(employeeMock.addEmployee(e));
    }

    @Order(7)
    @Test
    void addEmployee_TC7_BB() {
//      TC2_BVA	Armean	Ioan	2921120124598	LECTURER	5000	TRUE

        Employee e = new Employee();
        e.setLastName("Armean");
        e.setFirstName("Ioan");
        e.setCnp("2921120124598");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(5000.0);

        assertTrue(employeeMock.addEmployee(e));
    }

    @Order(8)
    @Test
    void addEmployee_TC8_BB() {
//      TC3_BVA	Zidaru	Mihai	2921120124598	TEACHER	4000	TRUE

        Employee e = new Employee();
        e.setLastName("Zidaru");
        e.setFirstName("Mihai");
        e.setCnp("2921120124598");
        e.setFunction(DidacticFunction.TEACHER);
        e.setSalary(5000.0);

        assertTrue(employeeMock.addEmployee(e));
    }

    @Order(9)
    @Test
    void addEmployee_TC9_BB() {
//      TC4_BVA	[iolpan	Diana	2921120124598	LECTURER	5000	FALSE

        Employee e = new Employee();
        e.setLastName("[iolpan");
        e.setFirstName("Diana");
        e.setCnp("2921120124598");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(5000.0);

        assertFalse(employeeMock.addEmployee(e));
    }

    @Order(10)
    @Test
    void addEmployee_TC10_BB() {
//     TC5_BVA	Ciolpan	@iana	2921120124598	LECTURER	5000	FALSE

        Employee e = new Employee();
        e.setLastName("Ciolpan");
        e.setFirstName("@iana");
        e.setCnp("2921120124598");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(5000.0);

        assertFalse(employeeMock.addEmployee(e));
    }

    @Order(11)
    @Test
    void addEmployee_TC11_BB() {
//     TC6_BVA	Armean	Ana	2921120124598	LECTURER	5000	TRUE

        Employee e = new Employee();
        e.setLastName("Armean");
        e.setFirstName("Ana");
        e.setCnp("2921120124598");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(5000.0);

        assertTrue(employeeMock.addEmployee(e));
    }

    @Order(12)
    @Test
    void addEmployee_TC12_BB() {
//     TC7_BVA	Ionescu	Zita	2921120124598	TEACHER	4000	TRUE

        Employee e = new Employee();
        e.setLastName("Ionescu");
        e.setFirstName("Zita");
        e.setCnp("2921120124598");
        e.setFunction(DidacticFunction.TEACHER);
        e.setSalary(4000.0);

        assertTrue(employeeMock.addEmployee(e));
    }

    @Order(13)
    @Test
    void addEmployee_TC13_BB() {
//     TC8_BVA	Popescu	[ndrei	2921120124598	LECTURER	5000	FALSE

        Employee e = new Employee();
        e.setLastName("Popescu");
        e.setFirstName("[ndrei");
        e.setCnp("2921120124598");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(5000.0);

        assertFalse(employeeMock.addEmployee(e));
    }

    @Order(14)
    @Test
    void addEmployee_TC14_BB() {
//     TC11_BVA	Ciolpan	Diana	29211201245985	LECTURER	5000	FALSE

        Employee e = new Employee();
        e.setLastName("Ciolpan");
        e.setFirstName("Diana");
        e.setCnp("29211201245985");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(5000.0);

        assertFalse(employeeMock.addEmployee(e));
    }

    @Test
    void modifyEmployeeFunction_TC01() {

        EmployeeMock employeeMock = new EmployeeMock();
        List<Employee> employeeList = employeeMock.getEmployeeList();
        Employee employeeToModify = employeeList.get(1);
        employeeMock.modifyEmployeeFunction(employeeToModify,DidacticFunction.LECTURER);
        assertEquals(DidacticFunction.LECTURER,employeeToModify.getFunction());

    }

    @Test
    void modifyEmployeeFunction_TC02() {
        String message = "";
        try {
            employeeMock.modifyEmployeeFunction(e, DidacticFunction.ASISTENT);
        } catch (NullPointerException exception) {
            message = exception.getMessage();
        }
        System.out.println(message);
      //  assertEquals(DidacticFunction.ASISTENT,e.getFunction());
    }

    @Test
    void modifyEmployeeFunction_TC03() {

        List<Employee> employeeList = new ArrayList<>();
        Employee e = new Employee();
        e.setFirstName("Ion");
        e.setLastName("Dumitrescu");
        e.setFunction(DidacticFunction.LECTURER);
        employeeMock.modifyEmployeeFunction(e,DidacticFunction.ASISTENT);
        assertEquals(DidacticFunction.LECTURER,e.getFunction());

        //  assertEquals(DidacticFunction.ASISTENT,e.getFunction());

    }
}